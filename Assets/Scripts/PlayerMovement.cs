﻿using System;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ConstantVars
{
    public const byte xFlag = 0b1;
    public const byte yFlag = 0b10;
    public const byte zFlag = 0b100;
    public const byte precisionFactor = 10;
}


/// <summary>
/// The Main concept we used here to optimise the memory packet is that we have converted the float deltaDiffernce into short data types
/// Second thing we are cheking the changes in particular axis, and sending changes data for those axis only(Doing using bitwise flags)
/// The changes occured are stored into a byte in the first 3 bits(again to reduce the memory)
/// overall If at a time we are sending changes for all 3 axis at max we are using 48 bits
/// Average for two axis it is taking 40 bits
/// </summary>




public class PlayerMovement : MonoBehaviour
{
    private byte changesInAxis = 0;//we are saving chnages in a byte, like a switch to save memory()

    private List<short> parametersTosend;

    private float xMovement;//input value along x axis

    private float zMovement;//input value along z axis

    private Vector3 tempPos;//cache initial value

    private Vector3 moveDirection;//move direction

    private Vector3 normalizedMove;

    [SerializeField]
    private float movementSpeed;

    private RemotePlayer remotePlayer;

    private float elapsed;

    private float updateInterval;

    // Start is called before the first frame update
    private void Start()
    {
        updateInterval = 0.1f;
        remotePlayer = FindObjectOfType<RemotePlayer>();
        parametersTosend = new List<short>();

    }

    // Update is called once per frame
    private void Update()
    {
        elapsed += Time.deltaTime;
        tempPos = transform.position;

        xMovement = Input.GetAxis("Horizontal");
        zMovement = Input.GetAxis("Vertical");

        moveDirection = new Vector3(xMovement, 0, zMovement);
        normalizedMove = moveDirection.normalized;

        transform.Translate(normalizedMove * Time.deltaTime * movementSpeed);



        //streaming data for the remote player
        if (elapsed > updateInterval)
        {
            if (xMovement != 0 || zMovement != 0)
            {
                updateRemotePlayerPos();
            }
            elapsed = 0.0f;

        }
    }

    private void updateRemotePlayerPos()
    {

        if (tempPos.x != transform.position.x)
        {
            setFlag(ConstantVars.xFlag);
            float changeInX = (float)Mathf.Round((transform.position.x - remotePlayer.transform.position.x) * 100f) / 100f;
            parametersTosend.Add(ConvertToShortParameter(changeInX));

        }

        if (tempPos.y != transform.position.y)
        {
            setFlag(ConstantVars.yFlag);
            float changeInY = (float)Mathf.Round((transform.position.y - remotePlayer.transform.position.y) * 100f) / 100f;
            parametersTosend.Add(ConvertToShortParameter(changeInY));

        }
        if (tempPos.z != transform.position.z)
        {
            setFlag(ConstantVars.zFlag);
            float changeInZ = (float)Mathf.Round((transform.position.z - remotePlayer.transform.position.z) * 100f) / 100f;
            parametersTosend.Add(ConvertToShortParameter(changeInZ));

        }


        // Check List Count to call function accordingly

        int count = parametersTosend.Count;

        //we are checking here if changes are in all the three axis
        if (count == 3)
        {
            remotePlayer.SendData(parametersTosend[0], parametersTosend[1], parametersTosend[2]);
        }
        else if (count == 2)//changes are only in 2 axis so just send these 2 values
        {
            remotePlayer.SendData(parametersTosend[0], parametersTosend[1], changesInAxis);
        }
        else if (count == 1)// change occured in one axis
        {
            remotePlayer.SendData(parametersTosend[0], changesInAxis);
        }
        resetElements();
    }

    /// <summary>
    /// Set flag method is used to updated the bits in a byte 10000000=x axis, 11000000=y axis, 11100000=all the axis//did this just to save the memory
    /// </summary>
    /// <param name="flag"></param>
    void setFlag(byte flag)
    {
        changesInAxis |= flag;
    }

    /// <summary>
    /// this method is simply converting float values to short
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    short ConvertToShortParameter(float num)
    {
        short Var = Convert.ToInt16(num * (10 * ConstantVars.precisionFactor));
        return (Var);
    }

    void resetElements()
    {
        changesInAxis = 0;
        parametersTosend.Clear();
    }
}
