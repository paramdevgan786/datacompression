﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RemotePlayer : MonoBehaviour
{

    private float xPosition;
    private float yPosition;
    private float zPosition;
    private Vector3 newPlayerPos;
    [SerializeField]
    private float xPosOffset;
    private Vector3 originalPlayerPos;

    // Start is called before the first frame update
    private void Start()
    {
        originalPlayerPos = transform.position;//new Vector3(transform.position.x + xPosOffset, transform.position.y, transform.position.z);
    }

    // Update is called every frame
    private void Update()
    {
        newPlayerPos = originalPlayerPos;
        transform.position = Vector3.Lerp(transform.position, newPlayerPos, Time.deltaTime * 10f);//interpolation

    }

    public void SendData(short param1, short param2, short param3)
    {

        Debug.Log("recieved data in bits" + GetSizeInBits(sizeof(short) * 3));
        resetValues();
        float parseValueParam1 = Convert.ToSingle(param1) / 100;
        float parseValueParam2 = Convert.ToSingle(param2) / 100;
        float parseValueParam3 = Convert.ToSingle(param3) / 100;

        xPosition = parseValueParam1 + xPosOffset;
        yPosition = parseValueParam2;
        zPosition = parseValueParam3;

        originalPlayerPos = transform.position + new Vector3(xPosition, xPosition, zPosition);
    }

    public void SendData(short param1, short param2, byte byt)
    {
        Debug.Log("recieved data in Bits " + GetSizeInBits(sizeof(short) * 2 + 1));
        resetValues();
        float parseValueParam1 = Convert.ToSingle(param1) / 100;
        float parseValueParam2 = Convert.ToSingle(param2) / 100;

        if ((byt & ConstantVars.xFlag) != 0 && (byt & ConstantVars.yFlag) != 0)//changes occured along x axis
        {
            xPosition = parseValueParam1 + xPosOffset; yPosition = parseValueParam2;

        }
        else if ((byt & ConstantVars.yFlag) != 0 && (byt & ConstantVars.zFlag) != 0)//changes occured along y axis
        {
            yPosition = parseValueParam1; zPosition = parseValueParam2;

        }
        else if ((byt & ConstantVars.zFlag) != 0 && (byt & ConstantVars.xFlag) != 0)//changes occured along z axis
        {
            xPosition = parseValueParam1 + xPosOffset; zPosition = parseValueParam2;

        }

        originalPlayerPos = transform.position + new Vector3(xPosition, yPosition, zPosition);
    }

    public void SendData(short param1, byte byt)
    {
        Debug.Log("recieved data in Bits" + GetSizeInBits(sizeof(short) + 1));
        resetValues();
        float parseValue = Convert.ToSingle(param1) / 100;

        if ((byt & ConstantVars.xFlag) != 0)//changes occured along x axis
        {
            xPosition = parseValue + xPosOffset;

        }
        else if ((byt & ConstantVars.yFlag) != 0)//changes occured along y axis
        {
            yPosition = parseValue;

        }
        else if ((byt & ConstantVars.zFlag) != 0)//changes occured along z axis
        {
            zPosition = parseValue;

        }

        originalPlayerPos = transform.position + new Vector3(xPosition, yPosition, zPosition);
    }


    private int GetSizeInBits(int bytes)
    {
        int rBits = bytes * 8;
        return rBits;
    }

    private void resetValues()
    {
        xPosition = 0.0f;
        yPosition = 0.0f;
        zPosition = 0.0f;
    }
}
